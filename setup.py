from setuptools import setup

setup(name="fontconfig",
      version="0.2",
      description="CFFI bindings for fontconfig",
      author="Marco Giusti, Lincoln Puzey",
      license="MIT",
      url="https://bitbucket.org/signiq/fontconfig",
      zip_safe=False,
      py_modules=["fc"],
      cffi_modules=["fontconfig_cffi.py:ffi"],
      setup_requires=["cffi>=1.12.3", 'six'],
      install_requires=["cffi>=1.12.3", 'enum34;python_version<"3.4"', 'six'],
      keywords=["fontconfig", "cffi"],
      classifiers=[
          "Development Status :: 3 - Alpha",
          "Intended Audience :: Developers",
          "License :: OSI Approved :: MIT License",
          "Programming Language :: Python",
          "Programming Language :: Python :: 2",
          "Programming Language :: Python :: 2.7",
          "Programming Language :: Python :: 3",
          "Programming Language :: Python :: 3.6",
          "Programming Language :: Python :: 3.7",
          "Topic :: Software Development :: Libraries"
      ])
