from __future__ import absolute_import, division, print_function, unicode_literals

# Copyright (c) Marco Giusti
# See LICENSE for details.

import subprocess
import os.path
import six
from cffi import FFI


def relopen(relpath, mode="r"):
    fullpath = os.path.join(os.path.dirname(__file__), relpath)
    return open(fullpath, mode)


def from_file(relpath):
    with relopen(relpath) as fp:
        return fp.read()


def parse_include(byte_string):
    assert byte_string.startswith(b"-I")
    byte_string = byte_string[2:].strip()
    if six.PY2:
        return byte_string
    else:
        return str(byte_string, 'utf-8')


def get_freetype_includes():
    cflags = subprocess.check_output(["pkg-config", "--cflags", "freetype2"])
    return list(map(parse_include, cflags.split()))


ffi = FFI()
ffi.set_source(
    "_fontconfig",
    from_file("_fixure.h"),
    libraries=["fontconfig"],
    include_dirs=get_freetype_includes(),
)
ffi.cdef(from_file("_fontconfig.h"))
