VP2 = vp2
VP36 = vp36
VP37 = vp37

VP2ENV = vp2/bin/activate
VP36ENV = vp36/bin/activate
VP37ENV = vp37/bin/activate

VP2FC = vp2/lib/python2.7/site-packages/fontconfig.egg-link
VP36FC = vp36/lib/python3.6/site-packages/fontconfig.egg-link
VP37FC = vp37/lib/python3.7/site-packages/fontconfig.egg-link

vp2: vp2/bin/activate
vp36: vp36/bin/activate
vp37: vp37/bin/activate

$(VP2ENV):%/bin/activate:
	virtualenv --no-site-packages $*

$(VP36ENV):%/bin/activate:
	python3.6 -m venv $*

$(VP37ENV):%/bin/activate:
	python3.7 -m venv $*

$(VP2FC): vp2
	vp2/bin/pip install -e .

$(VP36FC): vp36
	vp36/bin/pip install -e .

$(VP37FC): vp37
	vp37/bin/pip install -e .

run-tests-2: $(VP2FC)
	./vp2/bin/python -3 -m unittest test_fc

run-tests-36: $(VP36FC)
	./vp36/bin/python -m unittest test_fc

run-tests-37: $(VP37FC)
	./vp37/bin/python -m unittest test_fc

run-tests-all: run-tests-2 run-tests-36 run-tests-37

shell-2: vp2
	./vp2/bin/python

shell-36: vp36
	./vp36/bin/python

shell-37: vp37
	./vp37/bin/python

clean:
	rm -rf vp2 vp36 vp37 build fontconfig.egg-info _fontconfig.abi3.so _fontconfig.so .eggs